package sample;

public class Grid {

    private char[] cases = {' ',' ',' ',' ',' ',' ',' ',' ',' '};

    enum Result {
        WIN,
        NOTHING,
        END
    }

    private Result theResult;


    public Grid() {
        this.theResult = Result.NOTHING;
    }

    public char[] getCases() {
        return cases;
    }

    public void addCase (char charPlayer, int numCase) {
        this.cases[numCase] = charPlayer;
        this.theResult = this.getResult();



    }

    public Result getResult() {

        if(this.cases[0] == this.cases[1] && this.cases[0] == this.cases[2] && this.cases[0] != ' ') return Result.WIN;
        if(this.cases[3] == this.cases[4] && this.cases[3] == this.cases[5] && this.cases[3] != ' ') return Result.WIN;
        if(this.cases[6] == this.cases[7] && this.cases[6] == this.cases[8] && this.cases[6] != ' ') return Result.WIN;
        if(this.cases[0] == this.cases[3] && this.cases[0] == this.cases[6] && this.cases[0] != ' ') return Result.WIN;
        if(this.cases[1] == this.cases[4] && this.cases[1] == this.cases[7] && this.cases[1] != ' ') return Result.WIN;
        if(this.cases[2] == this.cases[5] && this.cases[2] == this.cases[8] && this.cases[2] != ' ') return Result.WIN;
        if(this.cases[0] == this.cases[4] && this.cases[0] == this.cases[8] && this.cases[0] != ' ') return Result.WIN;
        if(this.cases[2] == this.cases[4] && this.cases[2] == this.cases[6] && this.cases[2] != ' ') return Result.WIN;


        for(char c : this.cases) {
            if(c == ' ')
                return Result.NOTHING;
        }
        return Result.END;

    }

    public Result getTheResult() {

        return theResult;
    }

}
