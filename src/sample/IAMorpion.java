package sample;

import java.util.*;

public class IAMorpion {

    private char[] cases = {' ',' ',' ',' ',' ',' ',' ',' ',' '};

    // nombre entre 0 et 8
    private int indCaseJouee = -1;

    private int[][] lignes;
    public char charJoueur = 'X';
    public char charIA = 'O';
    public char charVide = ' ';

    public IAMorpion() {
        this.lignes = new int[][]{
                {0, 1, 2},
                {3, 4, 5},
                {6, 7, 8},
                {0, 3, 6},
                {1, 4, 7},
                {2, 5, 8},
                {0, 4, 8},
                {2, 4, 6}
        };

    }

    public char[] getCases() {
        return cases;
    }

    // doit renvoyer un nombre entre 1 et 9
    public int joue() {

        this.indCaseJouee = -1;

        // premier coup au hasard si l'IA commence
        if(nbCasesVides() == 9) {
           this.indCaseJouee = coup1();
        }
        else {
            // si le joueur commence, on joue au centre si possible
           if (nbCasesVides() == 8) {
               // on joue on centre si c possible
               if(this.cases[4] == charVide) this.indCaseJouee = 4;
               // sinon on joue en diagonale 0
               else this.indCaseJouee = 0;
           } else {

               if (nbCasesVides() == 7) {
                   this.indCaseJouee = this.coup2();
               } else {
                   if(nbCasesVides() == 6) {
                       this.indCaseJouee = this.coup2defense();
                   }
                   else {
                       int finPossible = peutFinir();
                       if (finPossible != -1) return finPossible;
                       int blocage = doitBloquer(charJoueur);
                       if (blocage != -1) return blocage;
                       List<Integer> doubleSolution = getDoubleSolutions(charIA);
                       if (doubleSolution.size() > 0) return doubleSolution.get(0);

                       return anticipeDoubleSolutionJoueur();
                   }
               }
           }
        }

        if(this.indCaseJouee == -1) {
            int premiereCaseVide = getPremiereCaseVide();
            return premiereCaseVide;
        }
        else {
            return this.indCaseJouee;
        }

    }

    public int peutFinir () {

        for (int[] ligne : lignes) {
            int nbCharIA = 0;
            int nbCharVide = 0;
            int indCaseVide = -1;
            for (int cas : ligne) {
                if (this.cases[cas] == charIA) nbCharIA++;
                if (this.cases[cas] == charVide) {
                    nbCharVide++;
                    indCaseVide = cas;
                }
            }
            // ya pas mieux c gagne
            if (nbCharIA == 2 && nbCharVide == 1) {
                return indCaseVide;
            }
        }
        return -1;
    }

    public int doitBloquer(char quelJoueur) {

        for (int[] ligne : lignes) {
            int nbCharJoueur = 0;
            int nbCharVide = 0;
            int indCaseVide = -1;
            for (int cas : ligne) {
                if (this.cases[cas] == quelJoueur) nbCharJoueur++;
                if (this.cases[cas] == charVide) {
                    nbCharVide++;
                    indCaseVide = cas;
                }
            }
            // ya pas mieux c gagne
            if (nbCharJoueur == 2 && nbCharVide == 1) {
                // on bloque la sol du joueur
                return indCaseVide;
            }
        }
        return -1;
    }

    public int coup1() {

        List<Integer> casesDepart = Arrays.asList(0,2,6,8,4,4,4,4,4,4);
        Random random = new Random();
        int index = random.nextInt(casesDepart.size());
        return casesDepart.get(index);
    }

    public int coup2() {
        int retour = -1;

        int[] casesDiagonale = {0,2,6,8};
        int[] casesCote = {1,3,5,7};

        List<Integer> casesJoueur = getCasesJoueur();
        List<Integer> casesIA = getCasesIA();

        // premier coup IA au centre, que faire au 2e
        if(this.cases[4] == charIA) {
            if(contains(casesDiagonale,casesJoueur.get(0))) {
                if(casesJoueur.get(0) == 0) retour = 8;
                if(casesJoueur.get(0) == 2) retour = 6;
                if(casesJoueur.get(0) == 6) retour = 2;
                if(casesJoueur.get(0) == 8) retour = 0;
            }
            else {
                if(casesJoueur.get(0) == 1) retour = 6;
                if(casesJoueur.get(0) == 3) retour = 2;
                if(casesJoueur.get(0) == 5) retour = 6;
                if(casesJoueur.get(0) == 7) retour = 2;
            }
        }
        // premier coup IA en diag car jamais de cote que faire au 2e
        // si le joueur a coche le centre
        else if(casesJoueur.get(0) == 4) {

            List<Integer> casesSuivantes;
            switch(casesIA.get(0)) {
                case 0:
                    casesSuivantes = Arrays.asList(5,7,8,8);
                    break;
                case 2:
                    casesSuivantes = Arrays.asList(3,7,6,6);
                    break;
                case 6:
                    casesSuivantes = Arrays.asList(1,2,2,5);
                    break;
                default:
                    casesSuivantes = Arrays.asList(0,0,1,3);
                    break;
            }

            Random random = new Random();
            int index = random.nextInt(casesSuivantes.size());
            retour = casesSuivantes.get(index);
        }
        // si le joueur n'a pas joue au centre
        else {
            // si le joueur joue un cote on met au centre
            if(contains(casesCote, casesJoueur.get(0))) retour = 4;
            // si le joueur joue en coin
            else {
                //si les 2 joueurs sont dans 2 coins oppposes on joue sur un coin restant
                if(getCoinOppose(casesIA.get(0)) == casesJoueur.get(0)) {
                    if(contains(casesDiagonale, casesIA.get(0) + 2 )) retour = casesIA.get(0) + 2;
                    if(contains(casesDiagonale, casesIA.get(0) - 2 )) retour = casesIA.get(0) - 2;
                }
                //si les 2 joueurs sont dans 2 coins alignes on joue le coin oppose du coin de l'IA
                else {
                    retour = getCoinOppose(casesIA.get(0));
                }
            }
        }

        return retour;
    }

    public int coup2defense() {

        int blocage = doitBloquer(charJoueur);
        if(blocage != -1) return blocage;

        // si centre joueur => 0 = IA => si joueur = 8
        if(this.cases[0] == charIA && this.cases[4] == charJoueur && this.cases[8] == charJoueur)
            return 2;
        else {
            return anticipeDoubleSolutionJoueur();
        }
    }

    public int anticipeDoubleSolutionJoueur() {

        List<Integer> casesVides = getCasesVides();
        char[] casesTemp = this.cases.clone();

        int i=-1;
        boolean coupOK = false;
        // on recupere la case qui ne provoque pas une double solution

        do {
            i++;
            // si ca a pas marche avec la case d'avant on remet this.cases a l'etat initial
            if (i > 0) this.cases[casesVides.get(i - 1)] = charVide;
            this.cases[casesVides.get(i)] = charIA;

            if(doitBloquer(charIA) != -1) {
                if (!getDoubleSolutions(charJoueur).contains(doitBloquer(charIA))) coupOK = true;
            }
            else {
                if(getDoubleSolutions(charJoueur).size() == 0) coupOK = true;
                else coupOK = false;
            }


        }
        while(!coupOK);

        this.cases = casesTemp.clone();

        return casesVides.get(i);

    }

    public List<Integer> getDoubleSolutions(char quelJoueur) {

        List<int[]> lignesPossibles = new ArrayList<>();
        // lignes avec juste un char IA
        for (int[] ligne: this.lignes) {
            int nbChar = 0;
            int nbCharVide = 0;
            for (int cas: ligne) {
                if(this.cases[cas] == quelJoueur) nbChar++;
                if(this.cases[cas] == charVide) nbCharVide++;
            }
            if(nbChar == 1 && nbCharVide == 2) {
                lignesPossibles.add(ligne);
            }
        }

        List<Integer> casesVidesDansLignesPossibles = new ArrayList<>();

        for (int[] lignePossible:lignesPossibles) {
            for (int cas: lignePossible) {
                if(this.cases[cas] == charVide) casesVidesDansLignesPossibles.add(cas);
            }
        }

        List<Integer> caseCorrespondante = new ArrayList<>();
        for (Integer cas: casesVidesDansLignesPossibles) {
            // si une case est presente plusieurs fois
            if(Collections.frequency(casesVidesDansLignesPossibles, cas) > 1)
                caseCorrespondante.add(cas);
        }
        return caseCorrespondante;
    }

    public List<Integer> getCasesJoueur() {

        List<Integer> cases = new ArrayList<>();
        for(int i=0; i<9; i++) {
            if(this.cases[i] == charJoueur) cases.add(i);
        }
        return cases;
    }

    public List<Integer> getCasesIA() {

        List<Integer> cases = new ArrayList<>();
        for(int i=0; i<9; i++) {
            if(this.cases[i] == charIA) cases.add(i);
        }
        return cases;
    }

    public List<Integer> getCasesVides() {

        List<Integer> cases = new ArrayList<>();
        for(int i=0; i<9; i++) {
            if(this.cases[i] == charVide) cases.add(i);
        }
        return cases;
    }

    public int getPremiereCaseVide() {

        for(int i=0; i<9; i++) {
            if(this.cases[i] == charVide)
                return i;
        }

        return -1;
    }

    public int getCoinOppose (int cas) {

        switch(cas) {
            case 0:
                return 8;
            case 2:
                return 6;
            case 6:
                return 2;
            default:
                return 0;
        }
    }

    public int nbCasesVides() {
        int nb = 0;
        for (char cas: this.cases) {
            if(cas == charVide) nb++;
        }
        return nb;
    }

    public static boolean contains(final int[] array, final int v) {

        boolean result = false;

        for(int i : array){
            if(i == v){
                result = true;
                break;
            }
        }

        return result;
    }

    public void addCase(char ch, int numCase) {
        this.cases[numCase] = ch;
    }


}
