package sample;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

public class Index {

    @FXML
    private GridPane grille;

    @FXML
    private Label label1;

    private final Image croix = new Image(Main.class.getResourceAsStream("images/croix.png"));
    private final Image rond = new Image(Main.class.getResourceAsStream("images/rond.png"));

    private IAMorpion IA = new IAMorpion();
    private Grid grid = new Grid();

    private char nextChar = 'X';
    private char commence = 'X';

    @FXML
    private void demarrer () {

        // centre le label1 par rapport a la fenetre
        label1.setMaxWidth(Double.MAX_VALUE);
        AnchorPane.setLeftAnchor(label1, 0.0);
        AnchorPane.setRightAnchor(label1, 0.0);
        label1.setAlignment(Pos.CENTER);

        grille.setDisable(false);
        grille.setCursor(Cursor.HAND);

        // reinitialise la grille et les tableau de char
        for(int i=0; i<9; i++ ) {
            ImageView im = (ImageView) grille.getChildren().get(i);
            im.setImage(null);
            //IA(nextChar, indice);
            grid.getCases()[i] = ' ';
            IA.getCases()[i] = ' ';
            label1.setText("");
        }

        if(commence == 'X') {
            // nextchar pour cette partie
            nextChar = 'X';
            // commence pour la partie d'apres
            commence = 'O';
        }
        else {
            nextChar = 'O';
            commence = 'X';
            // donc ordi commence
            playIA();
        }

    }

    @FXML
    private void clicCase (MouseEvent e) {

        Node source = (Node)e.getTarget();
        Integer colIndex = GridPane.getColumnIndex(source);
        Integer rowIndex = GridPane.getRowIndex(source);

        if(rowIndex == null) rowIndex = 0;
        if(colIndex == null) colIndex = 0;

        int indice = rowIndex * 3 + colIndex;

        if(grid.getCases()[indice] == ' ') {
            ImageView imageView = (ImageView) grille.getChildren().get(indice);
            imageView.setImage(croix);

            IA.addCase(nextChar, indice);
            grid.addCase(nextChar, indice);

            nextChar = 'O';

            if( grid.getTheResult().equals(Grid.Result.WIN) ) {
                label1.setText("Vous avez gagné la partie");
                grille.setDisable(true);
            }
            else if( grid.getTheResult().equals(Grid.Result.END) ) {
                label1.setText("Partie terminée. Aucun vainqueur");
                grille.setDisable(true);
            }
            else {
                playIA();
            }
        }


    }

    public void playIA() {

        int number = IA.joue();

        ImageView imageView = (ImageView) grille.getChildren().get(number);
        imageView.setImage(rond);

        IA.addCase(nextChar, number);
        grid.addCase(nextChar, number);

        if( grid.getTheResult().equals(Grid.Result.WIN) ) {
            label1.setText("Vous avez perdu la partie");
            grille.setDisable(true);
        }
        else if( grid.getTheResult().equals(Grid.Result.END) ) {
            label1.setText("Partie terminée. Aucun vainqueur");
            grille.setDisable(true);
        }

        nextChar = 'X';

    }




}
